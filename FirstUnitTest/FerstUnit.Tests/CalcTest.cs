using System;
using Xunit;

namespace FerstUnit.Tests
{
    public class CalcTest
    {
        [Fact]
        public void Sum_test()
        {
            int x = 2;
            int y = 3;
            int expected = 5;

            FirstUnitTest.Calc c = new FirstUnitTest.Calc();
            int actual = c.Sum(x, y);

            Assert.Equal(expected, actual);

        }
    }
}
